package com.epam.rd.java.basic.task8.object;

public class Flower {
    private String name;
    private String origin;
    private String solid;

    private String stemColour;
    private String leafColour;

    private int aveLengthFlower;

    private int tempreture;
    private String lighting;
    private int watering;

    private String multiplying;

    public Flower(String name, String origin, String solid, String stemColour, String leafColour, int aveLengthFlower, int tempreture, String lighting, int watering, String multiplying) {
        this.name = name;
        this.origin = origin;
        this.solid = solid;
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLengthFlower = aveLengthFlower;
        this.tempreture = tempreture;
        this.lighting = lighting;
        this.watering = watering;
        this.multiplying = multiplying;
    }

    public Flower() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getSolid() {
        return solid;
    }

    public void setSolid(String solid) {
        this.solid = solid;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLengthFlower() {
        return aveLengthFlower;
    }

    public void setAveLengthFlower(int aveLengthFlower) {
        this.aveLengthFlower = aveLengthFlower;
    }

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public String isLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", solid='" + solid + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLengthFlower=" + aveLengthFlower +
                ", tempreture=" + tempreture +
                ", lighting='" + lighting + '\'' +
                ", watering=" + watering +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
