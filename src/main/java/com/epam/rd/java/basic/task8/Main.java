package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        // PLACE YOUR CODE HERE
        domController.raed();
        // sort (case 1)
        // PLACE YOUR CODE HERE
        domController.sortList();

        // save
        String outputXmlFile = "output.dom.xml";
        // PLACE YOUR CODE HERE
        domController.write(outputXmlFile);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        saxController.parseXML();
        // sort  (case 2)
        saxController.sortElements();
        // PLACE YOUR CODE HERE
        // save
        outputXmlFile = "output.sax.xml";
        // PLACE YOUR CODE HERE
        saxController.save(outputXmlFile);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        staxController.parseXML();
        // sort  (case 3)
        // PLACE YOUR CODE HERE
        staxController.sortElements();
        // save
        outputXmlFile = "output.stax.xml";
        // PLACE YOUR CODE HERE
        staxController.save(outputXmlFile);
    }

}
