package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.object.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class DOMController {

    private String xmlFileName;

    private List<Flower> flowers;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void raed() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new File(xmlFileName));

            NodeList nodeList = document.getElementsByTagName("flower");

            flowers = new ArrayList<>();

            for (int i = 0; i < nodeList.getLength(); i++) {
                flowers.add(getFlower(nodeList.item(i)));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write(String outPutPathFile) {

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("flowers");
            rootElement.setAttribute("xmlns", "http://www.nure.ua");
            rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
            doc.appendChild(rootElement);

            for (Flower flower : flowers) {
                Element flowerNode = doc.createElement("flower");
                rootElement.appendChild(flowerNode);

                Element name = doc.createElement("name");
                name.setTextContent(flower.getName());
                flowerNode.appendChild(name);

                Element solid = doc.createElement("soil");
                solid.setTextContent(flower.getSolid());
                flowerNode.appendChild(solid);

                Element origin = doc.createElement("origin");
                origin.setTextContent(flower.getOrigin());
                flowerNode.appendChild(origin);

                Element visualParameters = doc.createElement("visualParameters");
                flowerNode.appendChild(visualParameters);

                Element stemColour = doc.createElement("stemColour");
                stemColour.setTextContent(flower.getStemColour());
                visualParameters.appendChild(stemColour);

                Element leafColour = doc.createElement("leafColour");
                leafColour.setTextContent(flower.getLeafColour());
                visualParameters.appendChild(leafColour);

                Element aveLenFlower = doc.createElement("aveLenFlower");
                aveLenFlower.setTextContent(Integer.toString(flower.getAveLengthFlower()));
                aveLenFlower.setAttribute("measure", "cm");
                visualParameters.appendChild(aveLenFlower);

                Element growingTips = doc.createElement("growingTips");
                flowerNode.appendChild(growingTips);

                Element tempreture = doc.createElement("tempreture");
                tempreture.setTextContent(Integer.toString(flower.getTempreture()));
                tempreture.setAttribute("measure", "celcius");
                growingTips.appendChild(tempreture);

                Element lighting = doc.createElement("lighting");
                lighting.setAttribute("lightRequiring", flower.isLighting());
                growingTips.appendChild(lighting);

                Element watering = doc.createElement("watering");
                watering.setAttribute("measure", "mlPerWeek");
                watering.setTextContent(Integer.toString(flower.getWatering()));
                growingTips.appendChild(watering);

                Element multiplying = doc.createElement("multiplying");
                multiplying.setTextContent(flower.getMultiplying());
                flowerNode.appendChild(multiplying);

            }
            writeXml(doc, outPutPathFile);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void writeXml(Document doc, String outPutPathFile)
            throws TransformerException, FileNotFoundException {

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(doc);

        transformer.transform(source, new StreamResult(new FileOutputStream(outPutPathFile)));

    }

    public static Flower getFlower(Node node) {
        Flower flower = new Flower();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            flower.setName(getTagValue("name", element));
            flower.setOrigin(getTagValue("origin", element));
            flower.setSolid(getTagValue("soil", element));
            flower.setStemColour(getTagValue("stemColour", element));
            flower.setLeafColour(getTagValue("leafColour", element));
            flower.setAveLengthFlower(Integer.parseInt(getTagValue("aveLenFlower", element)));
            flower.setTempreture(Integer.parseInt(getTagValue("tempreture", element)));
            flower.setLighting(getTagAttributeValue("lighting", "lightRequiring", element));
            flower.setWatering(Integer.parseInt(getTagValue("watering", element)));
            flower.setMultiplying(getTagValue("multiplying", element));
        }
        return flower;
    }

    private static String getTagAttributeValue(String tag, String attribute, Element element) {
        return element.getElementsByTagName(tag).item(0).getAttributes().getNamedItem(attribute).getNodeValue();
    }

    private static String getTagValue(String tag, Element element) {
        return element.getElementsByTagName(tag).item(0).getTextContent();
    }

    public void sortList() {
        flowers.sort(new Comparator<Flower>() {
            @Override
            public int compare(Flower o1, Flower o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }

    public static void main(String[] args) {
        DOMController dm = new DOMController("invalideXML.xml");
        dm.raed();
        dm.sortList();
        dm.write("output.dom.xml");
    }

}
